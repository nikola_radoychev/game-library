import { init } from '@rematch/core';
import { throttle } from 'throttle-debounce';
import models from './rematch-models';
import { loadState, saveState } from './localStorage';

const initialState = loadState();

const store = init({
  models,
  redux: { initialState },
});

store.subscribe(
  throttle(1000, () => {
    const { users } = store.getState();
    const { loggedInUser, token } = users;
    const savedUsersData = { loggedInUser, token, users: [] };
    saveState({ users: savedUsersData, gameLibrary: true });
  })
);

export const { dispatch } = store;
export default store;
