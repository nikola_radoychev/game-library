import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Avatar,
} from '@material-ui/core';
import AccountIcon from '@material-ui/icons/AccountCircle';
import UserMenu from '../utils/layoutTools/headerTools/UserMenu';

const styles = theme => ({
  root: {
    width: '100%',
  },
  grow: {
    flexGrow: 1,
  },
  appBar: {
    paddingTop: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
  },
  logo: {
    marginRight: theme.spacing.unit * 1.5,
    marginLeft: -theme.spacing.unit,
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
    color: theme.palette.primary.contrastText,
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
});

class Header extends React.Component {
  state = {
    anchorEl: null,
  };

  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: null });
  };
  render() {
    const { classes, logOut, token } = this.props;
    const { anchorEl } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="static" className={classes.appBar}>
          <Toolbar>
            <Avatar className={classes.logo} src="/logo.png" />
            <Typography variant="h4" className={classes.sectionDesktop}>
              Game Library
            </Typography>
            <div className={classes.grow} />

            {token && (
              <IconButton
                aria-haspopup="true"
                onClick={this.handleProfileMenuOpen}
                color="inherit"
              >
                <AccountIcon />
              </IconButton>
            )}
          </Toolbar>
        </AppBar>
        <UserMenu
          isMenuOpen={anchorEl}
          anchorEl={anchorEl}
          onClose={this.handleMenuClose}
          logOut={logOut}
        />
      </div>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  logOut: PropTypes.func.isRequired,
  token: PropTypes.string,
};

const mapState = dispatch => ({
  token: dispatch.users.token,
});

const mapDispatch = dispatch => ({
  logOut: dispatch.users.logOut,
  updateGame: dispatch.games.updateGameAsync,
});

export default connect(
  mapState,
  mapDispatch
)(withStyles(styles)(Header));
