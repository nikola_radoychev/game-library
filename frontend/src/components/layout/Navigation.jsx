import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  Tooltip,
} from '@material-ui/core/';
import HomeIcon from '@material-ui/icons/Home';
import AboutIcon from '@material-ui/icons/Info';
import LoginIcon from '@material-ui/icons/VerifiedUser';
import RegisterIcon from '@material-ui/icons/AssignmentInd';
import FavouritesIcon from '@material-ui/icons/Favorite';
import WantsToPlayIcon from '@material-ui/icons/CheckCircle';
import ThemeMenu from '../utils/layoutTools/ThemeMenu';

const styles = theme => ({
  drawer: {
    width: theme.spacing.unit * 7 + 1,
    backgroundColor: theme.palette.primary.main,
    position: 'static',
  },
  icon: {
    color: theme.palette.primary.contrastText,
  },
});

const getNavigationLinks = token => {
  const commonLinks = [
    {
      link: '/',
      name: 'Home',
      icon: <HomeIcon />,
    },
    {
      link: '/about',
      name: 'About',
      icon: <AboutIcon />,
    },
  ];

  const userLinks = [
    {
      link: '/games/favourites',
      name: 'Favourites',
      icon: <FavouritesIcon />,
    },
    {
      link: '/games/wantstoplay',
      name: 'Wants to play',
      icon: <WantsToPlayIcon />,
    },
  ];

  const guestLinks = [
    {
      link: '/signup',
      name: 'Register',
      icon: <RegisterIcon />,
    },
    {
      link: '/signin',
      name: 'Login',
      icon: <LoginIcon />,
    },
  ];

  return token
    ? [...commonLinks, ...userLinks]
    : [...commonLinks, ...guestLinks];
};

const Navigation = props => {
  const { classes, token } = props;
  const navigationLinks = getNavigationLinks(token);
  return (
    <Drawer
      variant="permanent"
      classes={{
        paper: classes.drawer,
      }}
    >
      <List className={classes.list}>
        {navigationLinks.map((item, index) => (
          <Tooltip title={item.name} key={index}>
            <Link to={item.link}>
              <ListItem button>
                <ListItemIcon
                  className={classes.icon}
                  classes={{ root: classes.icon }}
                >
                  {item.icon}
                </ListItemIcon>
              </ListItem>
            </Link>
          </Tooltip>
        ))}
        <ThemeMenu />
      </List>
    </Drawer>
  );
};

Navigation.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapState = state => ({
  token: state.users.token,
});

export default connect(mapState)(withStyles(styles)(Navigation));
