import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Avatar, Tooltip, Typography } from '@material-ui/core';

const styles = theme => ({
  root: {
    width: '100%',
    height: 100,
    backgroundColor: theme.palette.primary.main,
  },
  text: {
    color: theme.palette.primary.contrastText,
    fontWeight: 500,
    marginTop: theme.spacing.unit,
  },
  avatar: {
    width: 60,
    height: 60,
  },
});

const Footer = ({ classes }) => {
  return (
    <Grid
      container
      className={classes.root}
      alignContent="center"
      alignItems="center"
    >
      <Grid item xs={12} xl={12} align="center">
        <Typography className={classes.text}>
          SoftUni - ReactJS Project by Nikola Radoychev - 2019
        </Typography>
      </Grid>
      <Grid item xl={12} xs={12} align="center" className={classes.icon}>
        <Tooltip title="BitBucket Repository">
          <a href=" https://bitbucket.org/BigNik0/game-library/src/master/">
            <Avatar className={classes.avatar} src="/repository.png" />
          </a>
        </Tooltip>
      </Grid>
    </Grid>
  );
};

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);
