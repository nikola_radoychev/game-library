import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import Header from './Header';
import { greenTheme } from '../../config/theme';
import AppRouter from '../../AppRouter';
import Navigation from './Navigation';
import Notifications from '../notifications/Notifications';
import Footer from './Footer';

const styles = {
  div: {
    display: 'flex',
  },
};

const Layout = ({ classes, currentTheme }) => (
  <MuiThemeProvider theme={currentTheme || greenTheme}>
    <Header />
    <Notifications />
    <div className={classes.div}>
      <Navigation />
      <AppRouter />
    </div>
    <Footer />
  </MuiThemeProvider>
);
Layout.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapState = state => ({
  currentTheme: state.themes.theme,
});

export default withRouter(connect(mapState)(withStyles(styles)(Layout)));
