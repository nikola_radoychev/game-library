import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Slide } from '@material-ui/core';
import Notification from './Notification';

const styles = theme => ({
  root: {
    position: 'absolute',
    zIndex: 9999,
    right: 10,
    top: 100,
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit * 0.1,
  },
  marginNotification: {
    marginBottom: theme.spacing.unit * 0.1,
  },
  notificationMessage: {
    maxWidth: 400,
  },
});

const Notifications = props => {
  const { notifications, removeNotification, classes } = props;

  return notifications.length > 0 ? (
    <div className={classes.root}>
      {notifications.map((message, index) => (
        <Slide
          in={true}
          direction="left"
          key={index}
          className={classes.marginNotification}
        >
          <Notification
            message={message.text}
            variant={message.variant}
            onClose={() => removeNotification(index)}
          />
        </Slide>
      ))}
    </div>
  ) : null;
};

const mapState = state => ({
  notifications: state.notifications.messages,
});

const mapDispatch = dispatch => ({
  removeNotification: dispatch.notifications.removeNotification,
});

Notifications.propTypes = {
  classes: PropTypes.object.isRequired,
  notifications: PropTypes.array.isRequired,
  removeNotification: PropTypes.func.isRequired,
};

export default connect(
  mapState,
  mapDispatch
)(withStyles(styles)(Notifications));
