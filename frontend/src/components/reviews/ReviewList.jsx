import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import Review from './Review';

const styles = theme => ({
  button: {
    textDecoration: 'none',
    color: 'white',
    textTransform: 'none',
    fontSize: 18,
  },
  appBar: {
    padding: theme.spacing.unit * 0.1,
    borderRadius: 5,
  },
});

class ReviewList extends React.Component {
  render() {
    const { reviews } = this.props;

    return (
      <div>
        {reviews.map((review, i) => (
          <Review key={i} review={review} />
        ))}
      </div>
    );
  }
}

ReviewList.propTypes = {
  classes: PropTypes.object.isRequired,
  reviews: PropTypes.array,
};

export default connect()(withStyles(styles)(ReviewList));
