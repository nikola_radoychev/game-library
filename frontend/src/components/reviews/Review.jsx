import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import {
  Avatar,
  ListItem,
  List,
  ListItemAvatar,
  ListItemText,
  Typography,
  Divider,
  IconButton,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Close';

const styles = theme => ({
  game: {
    fontWeight: 900,
  },

  listItemRoot: {
    paddingTop: 0,
    paddingBottom: 0,
  },
  listRoot: {
    padding: theme.spacing.unit * 0.25,
  },
});

const Review = props => {
  const { classes, review, loggedInUser, token, deleteReview } = props;

  return (
    <List classes={{ root: classes.listRoot }}>
      <Divider />
      <ListItem
        className={classes.listItem}
        classes={{ root: classes.listItemRoot }}
      >
        <ListItemAvatar>
          <Avatar alt={`${review.creator.name}`} src={review.game.imageUrl} />
        </ListItemAvatar>
        <ListItemText
          primary={'Reviewer - ' + review.creator.name}
          secondary={
            <Typography component="div">
              <Typography color="textPrimary" className={classes.game}>
                {`Game - ${review.game.name}`}
              </Typography>
              <Typography color="inherit" align="justify">
                {review.content}
              </Typography>
            </Typography>
          }
        />{' '}
        {token && loggedInUser.role === 'Admin' && (
          <IconButton onClick={() => deleteReview(review._id)}>
            <DeleteIcon />
          </IconButton>
        )}
      </ListItem>
    </List>
  );
};

Review.propTypes = {
  classes: PropTypes.object.isRequired,
  review: PropTypes.object.isRequired,
  token: PropTypes.object.isRequired,
  loggedInUser: PropTypes.object.isRequired,
  deleteReview: PropTypes.func.isRequired,
};

const mapState = state => ({
  loggedInUser: state.users.loggedInUser,
  token: state.users.token,
});
const mapDispatch = dispatch => ({
  deleteReview: dispatch.reviews.deleteReviewAsync,
});

export default connect(
  mapState,
  mapDispatch
)(withStyles(styles)(Review));
