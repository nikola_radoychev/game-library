import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { IconButton, Dialog, Slide } from '@material-ui/core';
import ReviewsIcon from '@material-ui/icons/Forum';
import AddReviewForm from './AddReviewForm';

const styles = theme => ({
  root: {
    padding: theme.spacing.uniit,
  },
});

const Transition = props => <Slide direction="up" {...props} />;

class AddReview extends React.Component {
  state = { open: false };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { game, classes } = this.props;
    const { open } = this.state;

    return (
      <React.Fragment>
        <IconButton onClick={this.handleClickOpen}>
          <ReviewsIcon />
        </IconButton>
        <Dialog
          maxWidth="md"
          fullWidth
          open={open}
          onClose={this.handleClose}
          TransitionComponent={Transition}
          className={classes.root}
        >
          <AddReviewForm game={game} onClose={this.handleClose} />
        </Dialog>
      </React.Fragment>
    );
  }
}

AddReview.propTypes = {
  classes: PropTypes.object.isRequired,
  game: PropTypes.object,
};

export default withStyles(styles)(AddReview);
