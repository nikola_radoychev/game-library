import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import {
  Button,
  TextField,
  Typography,
  AppBar,
  Toolbar,
} from '@material-ui/core';

const styles = theme => ({
  flex: {
    flex: 1,
  },
  textField: {
    paddingBottom: theme.spacing.unit,
  },
  appBar: {
    position: 'relative',
  },
  button: {
    margin: theme.spacing.unit,
    textTransform: 'none',
    fontWeight: 900,
  },
});

class AddReviewForm extends Component {
  state = {
    content: '',
  };

  onSubmit = async e => {
    e.preventDefault();
    const gameId = this.props.game._id;
    const review = await this.props.createReview({ ...this.state, gameId });
    if (!review) {
      return;
    }
    this.props.onClose();
  };

  onChange = e =>
    this.setState({
      [e.target.name]: e.target.value,
    });

  render() {
    const { content } = this.state;
    const { classes } = this.props;

    return (
      <React.Fragment>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <Typography variant="h5" color="inherit" className={classes.flex}>
              Add new review
            </Typography>
          </Toolbar>
        </AppBar>
        <form onSubmit={this.onSubmit}>
          <div>
            <TextField
              name="content"
              variant="outlined"
              multiline
              fullWidth
              rows="15"
              type="text"
              placeholder="Content"
              value={content}
              className={classes.textField}
              onChange={this.onChange}
              helperText={'Content'}
            />
          </div>
          <div>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.button}
            >
              Create Review
            </Button>
          </div>
        </form>
      </React.Fragment>
    );
  }
}

const mapDispatch = dispatch => ({
  createReview: dispatch.reviews.createReviewAsync,
});

AddReviewForm.propTypes = {
  classes: PropTypes.object.isRequired,
  game: PropTypes.object.isRequired,
  createReview: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default connect(
  null,
  mapDispatch
)(withStyles(styles)(AddReviewForm));
