import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Tabs, Tab } from '@material-ui/core';
import TabContainer from './TabContainer';

const styles = {
  tab: {
    textDecoration: 'none',
    textTransform: 'none',
    fontSize: 16,
  },
  appBar: {
    textDecoration: 'none',
  },
};

class NavigationComponent extends React.Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes, tabs, tabContainerViews, appBarStyles } = this.props;
    const { value } = this.state;
    return (
      <React.Fragment>
        <AppBar
          position="static"
          color={'secondary'}
          className={appBarStyles || classes.appBar}
        >
          <Tabs
            value={value}
            onChange={this.handleChange}
            variant="scrollable"
            scrollButtons="on"
          >
            {tabs.map((tab, index) => (
              <Tab
                key={index}
                className={classes.tab}
                label={tab.label}
                icon={tab.icon}
              />
            ))}
          </Tabs>
        </AppBar>
        <TabContainer>
          {tabContainerViews.find((_, index) => value === index)}
        </TabContainer>
      </React.Fragment>
    );
  }
}

NavigationComponent.propTypes = {
  classes: PropTypes.object.isRequired,
  tabContainerViews: PropTypes.array.isRequired,
  appBarStyles: PropTypes.string,
  tabs: PropTypes.array.isRequired,
};

export default withStyles(styles)(NavigationComponent);
