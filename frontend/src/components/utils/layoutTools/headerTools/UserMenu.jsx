import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { MenuItem, Menu } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  menuItem: {
    textDecoration: 'none',
    textTransform: 'none',
    outline: 'none',
  },
});
class UserMenu extends React.Component {
  onClick = () => {
    this.props.logOut();
    this.props.onClose();
  };

  onSelectProfile = () => {
    this.props.onClose();
  };
  render() {
    const { anchorEl, onClose, classes } = this.props;
    const isMenuOpen = !!anchorEl;

    return (
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMenuOpen}
        onClose={onClose}
      >
        <Link to="/profile" className={classes.menuItem}>
          <MenuItem className={classes.menuItem} onClick={this.onSelectProfile}>
            Profile
          </MenuItem>
        </Link>
        <Link to="/" className={classes.menuItem}>
          <MenuItem className={classes.menuItem} onClick={this.onClick}>
            Logout
          </MenuItem>
        </Link>
      </Menu>
    );
  }
}

UserMenu.propTypes = {
  classes: PropTypes.object.isRequired,
  anchorEl: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};

export default withStyles(styles)(UserMenu);
