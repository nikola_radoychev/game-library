import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import {
  Menu,
  MenuItem,
  ListItem,
  ListItemIcon,
  Tooltip,
} from '@material-ui/core';
import ChangeThemeIcon from '@material-ui/icons/ColorLens';
import {
  greenTheme,
  redTheme,
  orangeTheme,
  blueTheme,
  blackTheme,
} from '../../../config/theme';

const styles = theme => ({
  icon: {
    color: theme.palette.primary.contrastText,
  },
});

const themeTypes = {
  'Red Theme': redTheme,
  'Green Theme': greenTheme,
  'Orange Theme': orangeTheme,
  'Blue Theme': blueTheme,
  'Black Theme': blackTheme,
};

class ThemeMenu extends React.Component {
  state = {
    anchorEl: null,
  };

  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: null });
  };

  onSelectItem = name => {
    const type = themeTypes[name];
    const { changeTheme } = this.props;
    changeTheme(type);
    this.setState({ anchorEl: null });
  };

  render() {
    const { classes } = this.props;
    const { anchorEl } = this.state;
    const isMenuOpen = !!anchorEl;

    return (
      <React.Fragment>
        <Tooltip title="Change theme">
          <ListItem
            button
            onClick={this.handleProfileMenuOpen}
            className={classes.button}
          >
            <ListItemIcon
              className={classes.icon}
              classes={{ root: classes.icon }}
            >
              <ChangeThemeIcon />
            </ListItemIcon>
          </ListItem>
        </Tooltip>
        <Menu
          anchorEl={anchorEl}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={isMenuOpen}
          onClose={this.handleMenuClose}
        >
          {' '}
          {Object.keys(themeTypes).map(name => (
            <MenuItem
              key={name}
              className={classes.menuItem}
              onClick={() => this.onSelectItem(name)}
            >
              {name}
            </MenuItem>
          ))}
        </Menu>
      </React.Fragment>
    );
  }
}

ThemeMenu.propTypes = {
  classes: PropTypes.object.isRequired,
  changeTheme: PropTypes.func.isRequired,
};

const mapDispatch = dispatch => ({
  changeTheme: dispatch.themes.changeTheme,
});

export default connect(
  null,
  mapDispatch
)(withStyles(styles)(ThemeMenu));
