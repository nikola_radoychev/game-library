import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {
  Table,
  TableBody,
  TableHead,
  TableRow,
  TableCell,
} from '@material-ui/core';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 2,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
  button: {
    backgroundColor: theme.palette.secondary.dark,
    color: 'white',
    textDecoration: 'none',
    textTransform: 'none',
  },
  disabledButton: {
    textDecoration: 'none',
    textTransform: 'none',
  },

  head: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
    textAlign: 'center',
  },
  body: {
    fontSize: 14,
    textAlign: 'center',
  },
});

const ItemsTable = props => {
  const { classes, items, tableCellsValues, tableCells } = props;

  return (
    <div className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            {tableCells.map((cellName, index) => (
              <TableCell key={index} className={classes.head}>
                {cellName}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {items.map((item, i) => (
            <TableRow className={classes.row} key={i}>
              {tableCellsValues.map((attrib, index) => (
                <TableCell className={classes.body} key={index}>
                  {item[attrib]}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </div>
  );
};

ItemsTable.propTypes = {
  actionType: PropTypes.object,
  buttonActions: PropTypes.object,
  classes: PropTypes.object.isRequired,
  items: PropTypes.array,
  onRemoveItem: PropTypes.func,
  options: PropTypes.object,
  setTableItem: PropTypes.func,
  tableCells: PropTypes.array.isRequired,
  tableCellsValues: PropTypes.array.isRequired,
  transformers: PropTypes.object,
};

export default withStyles(styles)(ItemsTable);
