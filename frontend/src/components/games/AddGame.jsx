import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Button, TextField, Typography } from '@material-ui/core';

const styles = theme => ({
  paper: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit,
  },
  textField: {
    paddingBottom: theme.spacing.unit,
  },
  typography: {
    margin: theme.spacing.unit,
  },
  button: {
    textTransform: 'none',
    fontWeight: 900,
    color: theme.palette.primary.contrastText,
    backgroundColor: theme.palette.primary.dark,
  },
});

class EditGame extends Component {
  state = {
    name: null,
    description: null,
    imageUrl: null,
    ownerCompany: null,
    trailerUrl: null,
  };

  onSubmit = e => {
    e.preventDefault();

    this.props.createGame(this.state);
    this.props.onClose();
  };

  onChange = e =>
    this.setState({
      [e.target.name]: e.target.value,
    });

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.paper} align="center">
        <Typography variant="h5" align="center" className={classes.typography}>
          Add New Game
        </Typography>

        <form onSubmit={this.onSubmit}>
          <TextField
            name="name"
            variant="outlined"
            type="text"
            fullWidth
            placeholder="Title"
            className={classes.textField}
            onChange={this.onChange}
            helperText={'Title'}
          />
          <TextField
            name="ownerCompany"
            variant="outlined"
            type="text"
            fullWidth
            placeholder="Owner Company"
            className={classes.textField}
            onChange={this.onChange}
            helperText={'Owner Company'}
          />
          <TextField
            name="imageUrl"
            variant="outlined"
            type="text"
            fullWidth
            placeholder="Image"
            className={classes.textField}
            onChange={this.onChange}
            helperText={'Image'}
          />
          <TextField
            name="trailerUrl"
            variant="outlined"
            type="text"
            fullWidth
            placeholder="Trailer"
            className={classes.textField}
            onChange={this.onChange}
            helperText={'Trailer'}
          />
          <TextField
            name="description"
            variant="outlined"
            multiline
            rows="15"
            type="text"
            fullWidth
            placeholder="Description"
            className={classes.textField}
            onChange={this.onChange}
            helperText={'Description'}
          />
          <Button type="submit" variant="contained" className={classes.button}>
            Create Game
          </Button>
        </form>
      </div>
    );
  }
}

const mapState = state => ({
  game: state.games.game,
});

const mapDispatch = dispatch => ({
  createGame: dispatch.games.createGameAsync,
});

EditGame.propTypes = {
  classes: PropTypes.object.isRequired,
  game: PropTypes.object.isRequired,
  createGame: PropTypes.func.isRequired,
};

export default connect(
  mapState,
  mapDispatch
)(withStyles(styles)(EditGame));
