import React from 'react';
import PropTypes from 'prop-types';
import ReactPlayer from 'react-player';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { Grid, Typography, Divider } from '@material-ui/core';
import ReviewList from '../reviews/ReviewList';

const styles = theme => ({
  root: {
    width: '100%',
  },
  player: {
    margin: theme.spacing.unit,
  },
  description: {
    padding: theme.spacing.unit * 2,
    textAlign: 'justify',
  },
  title: {
    backgroundColor: theme.palette.primary.dark,
    color: theme.palette.primary.contrastText,
    borderRadius: 10,
    margin: theme.spacing.unit * 3,
  },
});

class Game extends React.Component {
  componentDidMount() {
    const { match, loadGame, loadReviews } = this.props;
    const { id } = match.params;
    loadGame(id);
    loadReviews(id);
  }
  render() {
    const { classes, game, reviews } = this.props;
    return (
      <div className={classes.root} align="center">
        <Typography variant="h3" className={classes.title}>
          {game.name}
        </Typography>
        <Divider />
        <Grid container>
          <Grid item xs={12} sm={6}>
            <ReactPlayer
              className={classes.player}
              width="100%"
              url={game.trailerUrl}
              playing
              align="center"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <Typography variant="h5">Reviews:</Typography>
            <ReviewList reviews={reviews} />
          </Grid>
        </Grid>
        <Divider />
        <Typography variant="h5">Description</Typography>
        <Typography className={classes.description}>
          {game.description}
        </Typography>
      </div>
    );
  }
}

Game.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  game: PropTypes.object.isRequired,
  loadGame: PropTypes.func.isRequired,
  loadReviews: PropTypes.func.isRequired,
};

const mapState = state => ({
  game: state.games.game,
  reviews: state.reviews.reviews,
});

const mapDispatch = dispatch => ({
  loadGame: dispatch.games.loadGameAsync,
  loadReviews: dispatch.reviews.loadGameReviewsAsync,
});

export default connect(
  mapState,
  mapDispatch
)(withStyles(styles, { withTheme: true })(Game));
