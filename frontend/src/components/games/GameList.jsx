import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FavouriteIcon from '@material-ui/icons/Favorite';
import WantsToPlayIcon from '@material-ui/icons/CheckCircle';
import EditIcon from '@material-ui/icons/Build';
import { Grid } from '@material-ui/core';
import AddReviewDialog from '../reviews/AddReviewDialog';

const styles = theme => ({
  card: {
    display: 'flex',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: '100%',
    paddingTop: '50%',
    height: 0,
    '&:hover': {
      filter: 'brightness(0.7)',
      cursor: 'pointer',
    },
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
  },
  playIcon: {
    height: 38,
    width: 38,
  },
  mainGrid: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
  favourite: {
    color: theme.palette.error.main,
  },
  wanted: {
    color: theme.palette.primary.light,
  },
});

class GameList extends React.Component {
  onClick = game => {
    this.props.history.push(`/game/${game._id}`);
  };

  onLikeGame = gameId => {
    this.props.addGameToFavourites(gameId);
  };

  onWantsToPlay = gameId => {
    this.props.addWantedGame(gameId);
  };
  render() {
    const {
      classes,
      games,
      token,
      loggedInUser,
      wantedGames,
      favouriteGames,
    } = this.props;

    return (
      <Grid
        container
        spacing={16}
        alignItems="center"
        alignContent="center"
        className={classes.mainGrid}
      >
        {games.map((game, i) => (
          <Grid item lg={4} md={6} sm={6} xs={12} key={i}>
            <Card className={classes.card}>
              <div className={classes.details}>
                <CardContent className={classes.content}>
                  <Typography component="h5" variant="h5">
                    {game.name}
                  </Typography>
                  <Typography variant="subtitle1" color="textSecondary">
                    {game.ownerCompany}
                  </Typography>
                </CardContent>
                <div className={classes.controls}>
                  {token && (
                    <React.Fragment>
                      <IconButton onClick={() => this.onLikeGame(game._id)}>
                        {favouriteGames.map(x => x._id).includes(game._id) ? (
                          <FavouriteIcon className={classes.favourite} />
                        ) : (
                          <FavouriteIcon />
                        )}
                      </IconButton>
                      <IconButton onClick={() => this.onWantsToPlay(game._id)}>
                        {wantedGames.map(x => x._id).includes(game._id) ? (
                          <WantsToPlayIcon className={classes.wanted} />
                        ) : (
                          <WantsToPlayIcon />
                        )}
                      </IconButton>
                      <AddReviewDialog game={game} />
                      {loggedInUser.role !== 'User' && (
                        <Link to={`/edit/${game._id}`}>
                          <IconButton>
                            <EditIcon />
                          </IconButton>
                        </Link>
                      )}
                    </React.Fragment>
                  )}
                </div>
              </div>
              <CardMedia
                align="right"
                className={classes.cover}
                image={game.imageUrl}
                title={game.name}
                onClick={() => this.onClick(game)}
              />
            </Card>
          </Grid>
        ))}
      </Grid>
    );
  }
}

GameList.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  games: PropTypes.array.isRequired,
  favouriteGames: PropTypes.array.isRequired,
  wantedGames: PropTypes.array.isRequired,
};

const mapDispatch = dispatch => ({
  addGameToFavourites: dispatch.games.addFavouriteGameAsync,
  addWantedGame: dispatch.games.addWantedGameAsync,
});
export default connect(
  null,
  mapDispatch
)(withStyles(styles, { withTheme: true })(GameList));
