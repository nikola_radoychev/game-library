import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import {
  Button,
  Grid,
  TextField,
  Typography,
  Divider,
} from '@material-ui/core';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  paper: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit,
  },
  textField: {
    paddingBottom: theme.spacing.unit,
  },
  typography: {
    margin: theme.spacing.unit,
  },
  gameInfo: {
    margin: theme.spacing.unit,
    fontSize: 14,
    textTransform: 'none',
  },
});

class EditGame extends Component {
  state = {
    name: null,
    description: null,
    imageUrl: null,
    ownerCompany: null,
    trailerUrl: null,
  };

  async componentDidMount() {
    const { loadGame, match } = this.props;
    const { id } = match.params;
    await loadGame(id);
    const {
      name,
      description,
      imageUrl,
      ownerCompany,
      trailerUrl,
    } = this.props.game;
    this.setState({ name, description, imageUrl, ownerCompany, trailerUrl });
  }

  onSubmit = async e => {
    e.preventDefault();
    const id = this.props.game._id;
    try {
      await this.props.editGame({ ...this.state, id });
    } catch (errorMessage) {
      this.setState({ error: errorMessage });
    }
  };

  onChange = e =>
    this.setState({
      [e.target.name]: e.target.value,
    });

  render() {
    const {
      name,
      imageUrl,
      description,
      ownerCompany,
      trailerUrl,
    } = this.state;
    const { classes, game } = this.props;

    const gameDetails = [
      `ID: ${game._id}`,
      `Title: ${game.name}`,
      <img src={game.imageUrl} alt={game.imageUrl} width="100%" />,
      `Description: ${game.description}`,
    ];

    return (
      <Grid container spacing={16} className={classes.paper}>
        <Grid item lg={6} md={12} className={classes.root}>
          <Typography
            variant="h5"
            align="center"
            className={classes.typography}
          >
            Edit Game
          </Typography>
          <form onSubmit={this.onSubmit}>
            <TextField
              name="name"
              variant="outlined"
              type="text"
              fullWidth
              placeholder="Title"
              value={name}
              className={classes.textField}
              onChange={this.onChange}
              helperText={'Title'}
            />
            <TextField
              name="ownerCompany"
              variant="outlined"
              type="text"
              fullWidth
              placeholder="Owner Company"
              value={ownerCompany}
              className={classes.textField}
              onChange={this.onChange}
              helperText={'Owner Company'}
            />
            <TextField
              name="imageUrl"
              variant="outlined"
              type="text"
              fullWidth
              placeholder="Image"
              value={imageUrl}
              className={classes.textField}
              onChange={this.onChange}
              helperText={'Image'}
            />
            <TextField
              name="trailerUrl"
              variant="outlined"
              type="text"
              fullWidth
              placeholder="Trailer"
              value={trailerUrl}
              className={classes.textField}
              onChange={this.onChange}
              helperText={'Trailer'}
            />
            <TextField
              name="description"
              variant="outlined"
              multiline
              rows="15"
              type="text"
              fullWidth
              placeholder="Description"
              value={description}
              className={classes.textField}
              onChange={this.onChange}
              helperText={'Description'}
            />
            <Button type="submit" variant="contained" color="primary">
              Save changes
            </Button>
          </form>
        </Grid>
        <Grid item lg={6} md={12} sm={12} xs={12} className={classes.root}>
          <Typography variant="h5" align="center">
            Current Game details:
          </Typography>
          {gameDetails
            .filter(e => !!e)
            .map((element, index) => (
              <div key={index}>
                <Typography variant="button" className={classes.gameInfo}>
                  {element}
                </Typography>
                {index < gameDetails.length - 1 && <Divider />}
              </div>
            ))}
        </Grid>
      </Grid>
    );
  }
}

const mapState = state => ({
  game: state.games.game,
});

const mapDispatch = dispatch => ({
  editGame: dispatch.games.updateGameAsync,
  loadGame: dispatch.games.loadGameAsync,
});

EditGame.propTypes = {
  classes: PropTypes.object.isRequired,
  game: PropTypes.object.isRequired,
  editGame: PropTypes.func.isRequired,
  loadGame: PropTypes.func.isRequired,
};

export default connect(
  mapState,
  mapDispatch
)(withStyles(styles)(EditGame));
