import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, Slide, Button } from '@material-ui/core';
import AddGame from './AddGame';

const styles = theme => ({
  root: {
    padding: theme.spacing.uniit,
  },
  button: {
    textTransform: 'none',
    fontWeight: 900,
    color: theme.palette.primary.contrastText,
    backgroundColor: theme.palette.primary.dark,
  },
});

const Transition = props => <Slide direction="up" {...props} />;

class AddGameDialog extends React.Component {
  state = { open: false };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    const { open } = this.state;

    return (
      <React.Fragment>
        <Button onClick={this.handleClickOpen} className={classes.button}>
          Add game
        </Button>
        <Dialog
          fullWidth
          open={open}
          onClose={this.handleClose}
          TransitionComponent={Transition}
          className={classes.root}
        >
          <AddGame onClose={this.handleClose} />
        </Dialog>
      </React.Fragment>
    );
  }
}

AddGameDialog.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AddGameDialog);
