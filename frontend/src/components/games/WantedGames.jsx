import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { IconButton, Avatar } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Close';
import Table from '../utils/generics/Table';

const styles = theme => ({
  button: {
    textTransform: 'none',
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.primary.contrastText,
    '&:hover': {
      backgroundColor: theme.palette.secondary.dark,
    },
  },
  icon: {
    color: theme.palette.secondary.main,
  },
});

const cells = ['image', 'Title', 'Owner Company', 'Delete from this list'];

const values = ['imageUrl', 'name', 'ownerCompany', 'action'];

class WantedGames extends React.Component {
  componentDidMount() {
    this.props.loadGames();
  }

  onRemoveItem = gameId => {
    this.props.removeWantedGame(gameId);
  };

  render() {
    const { games } = this.props;
    const tableItems = games.map(x => ({
      ...x,
      imageUrl: (
        <div align="center">
          <Avatar src={x.imageUrl} />
        </div>
      ),
      action: (
        <IconButton onClick={() => this.onRemoveItem(x._id)}>
          <DeleteIcon />
        </IconButton>
      ),
    }));
    return (
      <Table items={tableItems} tableCells={cells} tableCellsValues={values} />
    );
  }
}

const mapState = state => ({
  games: state.games.wantsToPlay,
});

const mapDispatch = dispatch => ({
  loadGames: dispatch.games.loadWantedGamesAsync,
  removeWantedGame: dispatch.games.removeWantedGameAsync,
});

WantedGames.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default connect(
  mapState,
  mapDispatch
)(withStyles(styles)(WantedGames));
