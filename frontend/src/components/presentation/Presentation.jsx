import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {
  Grid,
  Button,
  Typography,
  Paper,
  StepContent,
  StepLabel,
  Step,
  Stepper,
  List,
  ListItem,
  ListItemText,
} from '@material-ui/core';
import { getStepContent } from '../../config/presentation';

const styles = theme => ({
  root: {
    width: '90%',
  },
  button: {
    marginTop: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  actionsContainer: {
    marginBottom: theme.spacing.unit * 2,
  },
  resetContainer: {
    padding: theme.spacing.unit * 3,
  },
  content: {
    textAlign: 'justify',
  },
  image: {
    maxHeight: 500,
  },
  title: {
    textAlign: 'center',
  },
});

function getSteps() {
  return [
    'Backend architecture',
    'Backend authentication',
    'Frontend architecture',
    'UI - Material UI',
    'Rematch Models',
    'React Components',
    'Features',
    'User Roles',
    'Notifications and Erros',
    'Forms',
    'Pages',
  ];
}

class VerticalLinearStepper extends React.Component {
  state = {
    activeStep: 0,
  };

  handleNext = () => {
    this.setState(state => ({
      activeStep: state.activeStep + 1,
    }));
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  };

  handleReset = () => {
    this.setState({
      activeStep: 0,
    });
  };

  render() {
    const { classes } = this.props;
    const steps = getSteps();
    const { activeStep } = this.state;

    return (
      <div className={classes.root}>
        <Typography variant="h4" className={classes.title}>
          Project presentation
        </Typography>
        <Stepper activeStep={activeStep} orientation="vertical">
          {steps.map((label, index) => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
              <StepContent>
                <Grid container spacing={16}>
                  <Grid item xl={6} lg={6} md={12}>
                    <List className={classes.content}>
                      {getStepContent(index).text.map((e, i) => (
                        <ListItem key={i}>
                          <ListItemText primary={e} />
                        </ListItem>
                      ))}
                    </List>
                  </Grid>
                  <Grid item xl={6} lg={6} md={12}>
                    <img
                      className={classes.image}
                      src={getStepContent(index).image}
                      width="100%"
                      alt={label}
                    />
                  </Grid>
                </Grid>
                <div className={classes.actionsContainer}>
                  <div>
                    <Button
                      disabled={activeStep === 0}
                      onClick={this.handleBack}
                      className={classes.button}
                    >
                      Back
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={this.handleNext}
                      className={classes.button}
                    >
                      {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                    </Button>
                  </div>
                </div>
              </StepContent>
            </Step>
          ))}
        </Stepper>
        {activeStep === steps.length && (
          <Paper square elevation={0} className={classes.resetContainer}>
            <Typography>All steps completed - you&apos;re finished</Typography>
            <Button onClick={this.handleReset} className={classes.button}>
              Reset
            </Button>
          </Paper>
        )}
      </div>
    );
  }
}

VerticalLinearStepper.propTypes = {
  classes: PropTypes.object,
};

export default withStyles(styles)(VerticalLinearStepper);
