import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Close';
import Table from '../utils/generics/Table';
import ChangeUserRoleMenu from './ChangeUserRoleMenu';

const styles = theme => ({
  button: {
    textTransform: 'none',
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.primary.contrastText,
    '&:hover': {
      backgroundColor: theme.palette.secondary.dark,
    },
  },
  icon: {
    color: theme.palette.secondary.main,
  },
});

const cells = ['Id', 'Name', 'Email', 'Edit Role', 'Delete User'];

const values = ['_id', 'name', 'email', 'changeRole', 'delete'];

class ManageUsers extends React.Component {
  async componentDidMount() {
    await this.props.loadUsers();
  }

  render() {
    const { deleteUser, users, loggedInUser } = this.props;
    const tableItems = users.map(x => ({
      ...x,
      changeRole: <ChangeUserRoleMenu user={x} />,
      delete: loggedInUser._id !== x._id && (
        <IconButton
          onClick={() => {
            deleteUser(x._id);
          }}
        >
          <DeleteIcon />
        </IconButton>
      ),
    }));
    return (
      <React.Fragment>
        <Table
          items={tableItems}
          tableCells={cells}
          tableCellsValues={values}
        />
      </React.Fragment>
    );
  }
}

const mapState = state => ({
  users: state.users.users,
  loggedInUser: state.users.loggedInUser,
});

const mapDispatch = dispatch => ({
  loadUsers: dispatch.users.loadUsersAsync,
  deleteUser: dispatch.users.deleteUserAsync,
});

ManageUsers.propTypes = {
  classes: PropTypes.object.isRequired,
  loggedInUser: PropTypes.object.isRequired,
  loadUsers: PropTypes.func.isRequired,
  deleteUser: PropTypes.func.isRequired,
  users: PropTypes.array.isRequired,
};

export default connect(
  mapState,
  mapDispatch
)(withStyles(styles)(ManageUsers));
