import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { IconButton, Avatar } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Close';
import Table from '../utils/generics/Table';
import AddGameDialog from '../games/AddGameDialog';

const styles = theme => ({
  button: {
    textTransform: 'none',
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.primary.contrastText,
    '&:hover': {
      backgroundColor: theme.palette.secondary.dark,
    },
  },
  icon: {
    color: theme.palette.secondary.main,
  },
});

const gameCells = ['image', 'Title', 'Owner Company', 'Delete Game'];

const gameValues = ['imageUrl', 'name', 'ownerCompany', 'action'];

class ManageGames extends React.Component {
  componentDidMount() {
    this.props.loadGames();
  }

  onRemoveItem = gameId => {
    this.props.deleteGame(gameId);
  };

  render() {
    const { games } = this.props;
    const tableItems = games.map(x => ({
      ...x,
      imageUrl: (
        <div align="center">
          <Avatar src={x.imageUrl} />
        </div>
      ),
      action: (
        <IconButton onClick={() => this.onRemoveItem(x._id)}>
          <DeleteIcon />
        </IconButton>
      ),
    }));
    return (
      <React.Fragment>
        <Table
          items={tableItems}
          tableCells={gameCells}
          tableCellsValues={gameValues}
        />
        <div align="right">
          <AddGameDialog />
        </div>
      </React.Fragment>
    );
  }
}

const mapState = state => ({
  games: state.games.games,
});

const mapDispatch = dispatch => ({
  loadGames: dispatch.games.loadGamesAsync,
  deleteGame: dispatch.games.removeGameAsync,
});

ManageGames.propTypes = {
  classes: PropTypes.object.isRequired,
  loadGames: PropTypes.func.isRequired,
  deleteGame: PropTypes.func.isRequired,
};

export default connect(
  mapState,
  mapDispatch
)(withStyles(styles)(ManageGames));
