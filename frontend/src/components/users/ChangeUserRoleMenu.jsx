import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { IconButton, Button, Menu, MenuItem } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';

const styles = theme => ({
  root: {
    width: '100%',
  },
  grow: {
    flexGrow: 1,
  },
  button: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    fontWeight: 900,
    textTransform: 'none',
  },
});

const teamRoles = ['User', 'Moderator', 'Admin'];

class ChangeUserRoleMenu extends React.Component {
  state = {
    anchorEl: null,
    userRole: null,
  };

  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: null });
  };

  onSelectItem = userRole => {
    this.setState({ anchorEl: null, userRole });
  };

  onSaveRole = () => {
    const { userRole } = this.state;
    const { role, _id } = this.props.user;
    if (!userRole || userRole === role) {
      return;
    }
    this.props.changeUserRole({ role: userRole, id: _id });
  };

  render() {
    const { classes, user } = this.props;
    const { anchorEl, userRole } = this.state;
    const isMenuOpen = !!anchorEl;

    return (
      <div className={classes.root}>
        <Button onClick={this.handleProfileMenuOpen} className={classes.button}>
          {userRole || user.role}
        </Button>
        <Menu
          anchorEl={anchorEl}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={isMenuOpen}
          onClose={this.handleMenuClose}
        >
          {' '}
          {teamRoles.map(role => (
            <MenuItem
              key={role}
              className={classes.menuItem}
              onClick={() => this.onSelectItem(role)}
            >
              {role}
            </MenuItem>
          ))}
        </Menu>
        <IconButton onClick={this.onSaveRole}>
          <SaveIcon />
        </IconButton>
      </div>
    );
  }
}

ChangeUserRoleMenu.propTypes = {
  classes: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  changeUserRole: PropTypes.func.isRequired,
};

const mapDispatch = dispatch => ({
  changeUserRole: dispatch.users.changeUserRoleAsync,
});

export default connect(
  null,
  mapDispatch
)(withStyles(styles)(ChangeUserRoleMenu));
