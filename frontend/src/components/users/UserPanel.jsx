import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Paper, Grid } from '@material-ui/core';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import ListIcon from '@material-ui/icons/Forum';
import NavigationComponent from '../utils/navigations/NavigationComponent';
import UserInfo from './UserInfo';
import ManageUsers from './ManageUsers';
import ManageGames from './ManageGames';

const styles = theme => ({
  userInfo: {
    flexGrow: 1,
    width: '80%',
    height: '100%',
    backgroundColor: theme.palette.background.paper,
  },
});

const allTabs = [
  { label: 'Profile Info', icon: <PersonPinIcon /> },
  { label: 'Manage games', icon: <ListIcon /> },
  { label: 'Manage users', icon: <ListIcon /> },
];

const getUserTabs = user => [
  <UserInfo key={0} user={user} />,
  <ManageGames key={2} />,
  <ManageUsers user={user} key={1} />,
];
const ProfileNavigation = props => {
  if (!props.loggedInUser) {
    props.history.push('/');
    return null;
  }
  const { classes, loggedInUser } = props;
  const tabs = getUserTabs(loggedInUser);
  const currentTabs =
    (loggedInUser.role === 'Admin' && allTabs) ||
    (loggedInUser.role === 'Moderator' && allTabs.slice(0, 2)) ||
    allTabs.slice(0, 1);

  return (
    <Grid item xs={12} sm={12} className={classes.userInfo}>
      <Paper>
        <NavigationComponent tabs={currentTabs} tabContainerViews={tabs} />
      </Paper>
    </Grid>
  );
};

const mapState = state => ({
  loggedInUser: state.users.loggedInUser,
});

ProfileNavigation.propTypes = {
  classes: PropTypes.object.isRequired,
  loggedInUser: PropTypes.object,
};

export default connect(mapState)(withStyles(styles)(ProfileNavigation));
