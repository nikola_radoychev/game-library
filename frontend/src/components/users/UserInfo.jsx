import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import {
  Paper,
  Grid,
  Typography,
  List,
  ListItemIcon,
  ListItem,
  ListItemText,
} from '@material-ui/core';
import AccessIcon from '@material-ui/icons/Check';
import { accountFunctions } from '../../config/constants';

const styles = theme => ({
  userInfo: {
    flexGrow: 1,
    width: '100%',
    height: '100%',
    backgroundColor: theme.palette.background.paper,
  },
});

class UserInfo extends React.Component {
  componentDidMount() {
    const { loadLoggedInUser } = this.props;
    loadLoggedInUser();
  }
  render() {
    const { loggedInUser } = this.props;
    const userFields = ['_id', 'name', 'email', 'role'];
    const fieldsNames = ['ID', 'Name', 'Email', 'User role'];
    const profileFunctions =
      (loggedInUser.role === 'Admin' && accountFunctions.admin) ||
      (loggedInUser.role === 'Moderator' && accountFunctions.moderator) ||
      (loggedInUser.role === 'User' && accountFunctions.user);

    return (
      <Grid container spacing={16}>
        <Grid item lg={6} md={6} sm={12} xs={12}>
          <Typography variant="h5" align="center">
            User details:
          </Typography>
          <Paper>
            {userFields.map((x, i) => (
              <List key={x}>
                <ListItem>
                  <ListItemText
                    primary={fieldsNames[i]}
                    secondary={loggedInUser[x]}
                  />
                </ListItem>
              </List>
            ))}
          </Paper>
        </Grid>
        <Grid item lg={6} md={6} sm={12} xs={12}>
          <Typography variant="h5" align="center">
            User Functions:
          </Typography>
          <Paper>
            {profileFunctions.map(x => (
              <List key={x}>
                <ListItem>
                  <ListItemIcon>
                    <AccessIcon />
                  </ListItemIcon>
                  <ListItemText primary={x} />
                </ListItem>
              </List>
            ))}
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

UserInfo.propTypes = {
  loggedInUser: PropTypes.object.isRequired,
  loadLoggedInUser: PropTypes.func.isRequired,
};

const mapState = state => ({
  loggedInUser: state.users.loggedInUser,
});

const mapDispatch = dispatch => ({
  loadLoggedInUser: dispatch.users.loadLoggedInUserAsync,
});

export default connect(
  mapState,
  mapDispatch
)(withStyles(styles)(UserInfo));
