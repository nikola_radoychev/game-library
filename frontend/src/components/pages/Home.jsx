import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import GameList from '../games/GameList';

class Home extends React.Component {
  componentDidMount() {
    const {
      loadGames,
      loadFavouriteGames,
      loadWantedGames,
      loadLoggedInUser,
      token,
    } = this.props;
    loadGames();
    token && loadLoggedInUser();
    token && loadFavouriteGames();
    token && loadWantedGames();
  }
  render() {
    const {
      games,
      history,
      token,
      loggedInUser,
      favouriteGames,
      wantedGames,
    } = this.props;

    return (
      <GameList
        games={games}
        history={history}
        token={token}
        loggedInUser={loggedInUser}
        favouriteGames={favouriteGames}
        wantedGames={wantedGames}
      />
    );
  }
}

const mapState = state => ({
  loggedInUser: state.users.loggedInUser,
  token: state.users.token,
  games: state.games.games,
  favouriteGames: state.games.favouriteGames,
  wantedGames: state.games.wantsToPlay,
});

const mapDispatch = dispatch => ({
  loadGames: dispatch.games.loadGamesAsync,
  loadFavouriteGames: dispatch.games.loadFavouriteGamesAsync,
  loadWantedGames: dispatch.games.loadWantedGamesAsync,
  loadLoggedInUser: dispatch.users.loadLoggedInUserAsync,
});

Home.propTypes = {
  games: PropTypes.array.isRequired,
  favouriteGames: PropTypes.array.isRequired,
  wantedGames: PropTypes.array.isRequired,
  loadGames: PropTypes.func.isRequired,
  loadFavouriteGames: PropTypes.func.isRequired,
  loadWantedGames: PropTypes.func.isRequired,
  token: PropTypes.string,
  loggedInUser: PropTypes.object,
  loadLoggedInUser: PropTypes.func.isRequired,
};

export default connect(
  mapState,
  mapDispatch
)(Home);
