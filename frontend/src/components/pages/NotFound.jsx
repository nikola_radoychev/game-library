import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Typography, Divider, Button } from '@material-ui/core';

const styles = theme => ({
  typography: {
    marginTop: theme.spacing.unit * 10,
    color: theme.palette.error.main,
    textAlign: 'center',
    fontWeight: 900,
    width: '90%',
  },
  button: {
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2,
    backgroundColor: theme.palette.error.main,
    '&:hover': {
      backgroundColor: theme.palette.error.light,
    },
    color: theme.palette.primary.contrastText,
    width: 200,
    textTransform: 'none',
  },
  divider: {
    marginLeft: '5%',
    marginTop: theme.spacing.unit * 2,
  },
});

const NotFound = ({ classes, history }) => {
  return (
    <Typography className={classes.typography} variant="h2">
      Page not Found! 404
      <Divider className={classes.divider} />
      <Button
        className={classes.button}
        onClick={() => {
          history.push('/');
        }}
      >
        Go to home page!
      </Button>
    </Typography>
  );
};

NotFound.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withStyles(styles)(NotFound);
