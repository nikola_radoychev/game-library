import { fetcher } from './utils/fetcher';
import {
  serverDown,
  getNotificationMessage,
  passwordDoesNotMatchMessage,
  invalidEmailMessage,
  shortPasswordMessage,
  deleteUserMesage,
  changeUserRoleMessage,
} from '../config/notifications';
import validator from 'validator';

const initialState = {
  users: [],
  user: null,
  loggedInUser: null,
  token: null,
};

export default {
  state: initialState,
  reducers: {
    loadUser(state, user) {
      return {
        ...state,
        user,
      };
    },

    loadUsers(state, users) {
      return {
        ...state,
        users,
      };
    },

    setLoggedInUser(state, loggedInUser, token) {
      return {
        ...state,
        loggedInUser,
        token,
      };
    },

    logOut() {
      return {
        ...initialState,
      };
    },

    clearAll() {
      return { ...initialState };
    },
  },
  effects: dispatch => {
    const setError = err => {
      if (!err.response) {
        dispatch.notifications.addNotificationMessage(serverDown);
        return;
      }
      dispatch.notifications.addNotificationMessage(
        getNotificationMessage(err.message, 'error')
      );
    };
    return {
      async loadUsersAsync(_, rootState) {
        const { token } = rootState.users;
        try {
          const users = await fetcher(`/rest/users/all`, { token });
          this.loadUsers(users);
        } catch (err) {
          setError(err);
          this.loadUsers([]);
        }
      },

      async deleteUserAsync(id, rootState) {
        const { token } = rootState.users;
        try {
          await fetcher.delete(`/rest/users/${id}`, { token });
          this.loadUsersAsync();
          dispatch.notifications.addNotificationMessage(
            getNotificationMessage(deleteUserMesage, 'info')
          );
        } catch (err) {
          setError(err);
        }
      },

      async changeUserRoleAsync(data, rootState) {
        const { token } = rootState.users;
        try {
          await fetcher.patch(`/rest/users/changerole`, data, { token });
          this.loadUsersAsync();
          dispatch.notifications.addNotificationMessage(
            getNotificationMessage(changeUserRoleMessage, 'info')
          );
        } catch (err) {
          setError(err);
        }
      },

      async registerUserAsync(data) {
        try {
          const hasError = {};
          hasError.value = false;
          const { email, password } = data;
          if (!validator.isEmail(email)) {
            dispatch.notifications.addNotificationMessage(
              getNotificationMessage(invalidEmailMessage, 'warning')
            );
            hasError.value = true;
          }
          if (password.length < 6) {
            dispatch.notifications.addNotificationMessage(
              getNotificationMessage(shortPasswordMessage, 'warning')
            );
            hasError.value = true;
          }
          if (password !== data.repeatPassword) {
            dispatch.notifications.addNotificationMessage(
              getNotificationMessage(passwordDoesNotMatchMessage, 'warning')
            );
            hasError.value = true;
          }
          if (hasError.value) {
            return;
          }
          const user = await fetcher.post(`/rest/users/create`, data);
          const result = await fetcher.post(`/rest/auth/login`, {
            email,
            password,
          });
          this.setLoggedInUser(user, result.token);
          dispatch.notifications.addNotificationMessage(
            getNotificationMessage(`Welcome ${user.name}`, 'success')
          );
        } catch (err) {
          setError(err);
        }
      },

      async loginUserAsync(data) {
        try {
          const hasError = {};
          hasError.value = false;
          if (!validator.isEmail(data.email)) {
            dispatch.notifications.addNotificationMessage(
              getNotificationMessage(invalidEmailMessage, 'warning')
            );
            hasError.value = true;
          }
          if (data.password.length < 6) {
            dispatch.notifications.addNotificationMessage(
              getNotificationMessage(shortPasswordMessage, 'warning')
            );
            hasError.value = true;
          }
          if (hasError.value) {
            return;
          }
          const { token } = await fetcher.post(`/rest/auth/login`, data);
          const loggedInUser = await fetcher.get('/rest/auth/profile', {
            token,
          });
          this.setLoggedInUser(loggedInUser, token);
          dispatch.notifications.addNotificationMessage(
            getNotificationMessage(`Welcome ${loggedInUser.name}`, 'success')
          );
        } catch (err) {
          setError(err);
        }
      },

      async loadLoggedInUserAsync(_, rootState) {
        try {
          const { token } = rootState.users;
          const loggedInUser = await fetcher.get('/rest/auth/profile', {
            token,
          });
          this.setLoggedInUser(loggedInUser, token);
        } catch (err) {
          setError(err);
        }
      },
    };
  },
};
