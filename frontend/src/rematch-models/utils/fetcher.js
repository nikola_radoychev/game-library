import fetch from 'unfetch';

const baseUrl = 'http://localhost:5050';

const defaultOptions = {
  method: 'GET',
  mode: 'cors',
  headers: {
    'Content-Type': 'application/json; charset=utf-8',
    Accept: 'application/json',
  },
};

const getFetchOptions = (options = {}) => {
  const { currentOptions } = options;
  const result = {
    ...defaultOptions,
    ...currentOptions,
    headers: {
      ...defaultOptions.headers,
      ...(currentOptions && currentOptions.headers),
    },
  };

  if (options.token) {
    result.headers.Authorization = `Bearer ${options.token}`;
  }

  return result;
};

const fetcher = async (path, options) => {
  const res = await fetch(baseUrl + path, getFetchOptions(options));
  const error = Object.assign(new Error(res.statusText), {
    response: res,
    status: res.status,
  });
  try {
    const data = await res.json();
    if (!res.ok) {
      error.message = data.error;
      throw error;
    }
    return data;
  } catch (_) {
    throw error;
  }
};

['get', 'delete'].forEach(method => {
  fetcher[method] = (path, options) =>
    fetcher(path, {
      currentOptions: {
        method: method.toUpperCase(),
        ...(options && options.currentOptions),
      },
      ...options,
    });
});

['post', 'put', 'patch'].forEach(method => {
  fetcher[method] = (path, data, options) =>
    fetcher(path, {
      currentOptions: {
        method: method.toUpperCase(),
        body: data ? JSON.stringify(data) : '',
        ...(options && options.currentOptions),
      },
      ...options,
    });
});

const queryBuilder = queries =>
  Object.entries(queries)
    .map(x => x.map(encodeURIComponent).join('='))
    .join('&');

export { fetcher, baseUrl, queryBuilder };
