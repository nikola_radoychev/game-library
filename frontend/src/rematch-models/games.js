import { fetcher } from './utils/fetcher';
import {
  serverDown,
  getNotificationMessage,
  updateGameMessage,
  createGameMessage,
  removeGameMessage,
} from '../config/notifications';

const initialState = {
  games: [],
  favouriteGames: [],
  wantsToPlay: [],
  game: {},
};

export default {
  state: initialState,
  reducers: {
    loadGames(state, games) {
      return {
        ...state,
        games,
      };
    },

    loadFavouriteGames(state, favouriteGames) {
      return {
        ...state,
        favouriteGames,
      };
    },

    loadWantedGames(state, wantsToPlay) {
      return {
        ...state,
        wantsToPlay,
      };
    },

    loadGame(state, game) {
      return {
        ...state,
        game,
      };
    },

    clearAll() {
      return { ...initialState };
    },
  },

  effects: dispatch => {
    const setError = err => {
      if (!err.response) {
        dispatch.notifications.addNotificationMessage(serverDown);
        return;
      }
      dispatch.notifications.addNotificationMessage(
        getNotificationMessage(err.message, 'error')
      );
    };

    return {
      async loadGamesAsync() {
        try {
          const games = await fetcher(`/rest/games/all`);
          this.loadGames(games);
        } catch (err) {
          setError(err);
        }
      },

      async loadFavouriteGamesAsync(_, rootState) {
        const { token } = rootState.users;
        try {
          const favouriteGames = await fetcher(`/rest/games/favourites`, {
            token,
          });
          this.loadFavouriteGames(favouriteGames);
        } catch (err) {
          setError(err);
        }
      },

      async loadWantedGamesAsync(_, rootState) {
        const { token } = rootState.users;
        try {
          const games = await fetcher(`/rest/games/wantstoplay`, {
            token,
          });

          this.loadWantedGames(games);
        } catch (err) {
          setError(err);
        }
      },

      async loadGameAsync(id) {
        try {
          const game = await fetcher(`/rest/games/${id}`);
          this.loadGame(game);
        } catch (err) {
          setError(err);
        }
      },

      async updateGameAsync(data, rootState) {
        const { token } = rootState.users;
        try {
          console.log(data);
          console.log('here');
          const game = await fetcher.patch(`/rest/games/update`, data, {
            token,
          });
          this.loadGameAsync(game._id);
          dispatch.notifications.addNotificationMessage(
            getNotificationMessage(updateGameMessage, 'success')
          );
        } catch (err) {
          setError(err);
        }
      },

      async createGameAsync(data, rootState) {
        const { token } = rootState.users;
        try {
          const game = await fetcher.post(`/rest/games/create`, data, {
            token,
          });
          this.loadGameAsync(game._id);
          this.loadGamesAsync();
          dispatch.notifications.addNotificationMessage(
            getNotificationMessage(createGameMessage, 'success')
          );
        } catch (err) {
          setError(err);
        }
      },

      async addFavouriteGameAsync(data, rootState) {
        const { token } = rootState.users;
        try {
          await fetcher.post(
            `/rest/games/add/favourite`,
            { gameId: data },
            {
              token,
            }
          );
          this.loadFavouriteGamesAsync();
        } catch (err) {
          setError(err);
        }
      },

      async removeGameAsync(id, rootState) {
        try {
          const { token } = rootState.users;
          await fetcher.delete(`/rest/games/${id}`, { token });
          this.loadGamesAsync();
          dispatch.notifications.addNotificationMessage(
            getNotificationMessage(removeGameMessage, 'info')
          );
        } catch (err) {
          setError(err);
        }
      },

      async removeFavouriteGameAsync(data, rootState) {
        const { token } = rootState.users;
        try {
          await fetcher.post(
            `/rest/games/remove/favourite`,
            { gameId: data },
            {
              token,
            }
          );
          this.loadFavouriteGamesAsync();
        } catch (err) {
          setError(err);
        }
      },

      async addWantedGameAsync(data, rootState) {
        const { token } = rootState.users;
        try {
          await fetcher.post(
            `/rest/games/add/wantstoplay`,
            { gameId: data },
            {
              token,
            }
          );
          this.loadWantedGamesAsync();
        } catch (err) {
          setError(err);
        }
      },

      async removeWantedGameAsync(data, rootState) {
        const { token } = rootState.users;
        try {
          await fetcher.post(
            `/rest/games/remove/wantstoplay`,
            { gameId: data },
            {
              token,
            }
          );
          this.loadWantedGamesAsync();
        } catch (err) {
          setError(err);
        }
      },
    };
  },
};
