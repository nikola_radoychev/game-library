import games from './games';
import users from './users';
import reviews from './reviews';
import notifications from './notifications';
import themes from './themes';

export default { games, users, reviews, notifications, themes };
