import { fetcher } from './utils/fetcher';
import {
  serverDown,
  getNotificationMessage,
  shortReviewMessage,
  createReviewMessage,
  removeReviewMessage,
} from '../config/notifications';

const initialState = {
  reviews: [],
  review: {},
};

export default {
  state: initialState,
  reducers: {
    loadReviews(state, reviews) {
      return {
        ...state,
        reviews,
      };
    },

    loadReview(state, review) {
      return {
        ...state,
        review,
      };
    },

    clearAll() {
      return { ...initialState };
    },
  },

  effects: dispatch => {
    const setError = err => {
      if (!err.response) {
        dispatch.notifications.addNotificationMessage(serverDown);
        return;
      }
      dispatch.notifications.addNotificationMessage(
        getNotificationMessage(err.message, 'error')
      );
    };

    return {
      async createReviewAsync(data, rootState) {
        const { token } = rootState.users;
        try {
          if (data.content.length < 20) {
            dispatch.notifications.addNotificationMessage(
              getNotificationMessage(shortReviewMessage, 'warning')
            );
            return;
          }
          const review = await fetcher.post(`/rest/reviews/create`, data, {
            token,
          });
          dispatch.notifications.addNotificationMessage(
            getNotificationMessage(createReviewMessage, 'success')
          );
          return review;
        } catch (err) {
          console.log(err);
          setError(err);
        }
      },
      async loadGameReviewsAsync(id, rootState) {
        const { token } = rootState.users;
        try {
          const reviews = await fetcher.get(`/rest/reviews/game/${id}`, {
            token,
          });
          this.loadReviews(reviews);
        } catch (err) {
          setError(err);
        }
      },

      async deleteReviewAsync(id, rootState) {
        const { token } = rootState.users;
        const gameId = rootState.games.game._id;
        try {
          await fetcher.delete(`/rest/reviews/${id}`, {
            token,
          });
          this.loadGameReviewsAsync(gameId);
          dispatch.notifications.addNotificationMessage(
            getNotificationMessage(removeReviewMessage, 'info')
          );
        } catch (err) {
          setError(err);
        }
      },
    };
  },
};
