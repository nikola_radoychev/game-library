const initialState = {
  messages: [],
};

export default {
  state: initialState,
  reducers: {
    addNotificationMessage(state, message) {
      return {
        ...state,
        messages: [...new Set([...state.messages, message])],
      };
    },

    removeNotification(state, index) {
      const { messages } = state;

      return {
        ...state,
        messages: [
          ...messages.slice(0, index),
          ...messages.slice(index + 1, messages.length),
        ],
      };
    },
  },
};
