const initialState = {
  theme: null,
};

export default {
  state: initialState,
  reducers: {
    changeTheme(state, theme) {
      return {
        ...state,
        theme,
      };
    },
  },
};
