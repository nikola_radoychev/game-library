import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './components/pages/Home';
import SignIn from './components/users/auth/SignIn';
import SignUp from './components/users/auth/SignUp';
import Game from './components/games/Game';
import EditGame from './components/games/EditGame';
import FavouriteGames from './components/games/FavouriteGames';
import WantedGames from './components/games/WantedGames';
import UserPanel from './components/users/UserPanel';
import Presentation from './components/presentation/Presentation';
import NotFound from './components/pages/NotFound';

const AppRouter = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route exact path="/signin" component={SignIn} />
    <Route exact path="/signup" component={SignUp} />
    <Route exact path="/profile" component={UserPanel} />
    <Route exact path="/about/" component={Presentation} />
    <Route exact path="/game/:id" component={Game} />
    <Route exact path="/games/favourites" component={FavouriteGames} />
    <Route exact path="/games/wantstoplay" component={WantedGames} />
    <Route exact path="/edit/:id" component={EditGame} />
    <Route component={NotFound} />
  </Switch>
);

export default AppRouter;
