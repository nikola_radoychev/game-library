// Error notifications
export const serverDown = {
  text: 'Server Error. Please refresh the page and try again.',
  variant: 'error',
};

export const getNotificationMessage = (text, variant) => ({ text, variant });

export const shortPasswordMessage = 'Password should be at least 6 symbols';

export const invalidEmailMessage = 'This is not a valid email';

export const passwordDoesNotMatchMessage = 'Password does not match';

export const invalidData = 'Invalid data';

export const createGameMessage = 'A game is successfully created';

export const updateGameMessage = 'The game is successfully updated ';

export const createReviewMessage = 'The reviews is successfully created ';

export const deleteUserMesage = 'The user is deleted';

export const changeUserRoleMessage = 'The user role is updated';

export const shortReviewMessage = 'Review should be at least 20 symbols';

export const removeGameMessage = 'The game is removed';

export const removeReviewMessage = 'The review is removed';
