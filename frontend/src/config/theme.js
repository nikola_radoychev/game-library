import { createMuiTheme } from '@material-ui/core/styles';

export const greenTheme = createMuiTheme({
  palette: {
    primary: {
      light: '#4DB6AC',
      main: '#00695C',
      dark: '#004D40',
      contrastText: '#fff',
    },
    secondary: {
      light: '#2db999',
      main: '#25947b',
      dark: '#185f4f',
      contrastText: '#fff',
    },
    warning: {
      main: '#ffa000',
      contrastText: '#fff',
    },
    success: {
      main: '#43a047',
      contrastText: '#fff',
    },
    info: {
      main: '#1976d2',
      contrastText: '#fff',
    },
    background: {
      main: '#EEEEEE',
    },
  },
  typography: {
    useNextVariants: true,
  },
});

export const redTheme = createMuiTheme({
  palette: {
    primary: {
      light: '#d8463c',
      main: '#b7372e',
      dark: '#73231d',
      contrastText: '#fff',
    },
    secondary: {
      light: '#e44646',
      main: '#881e1e',
      dark: '#7d2424',
      contrastText: '#fff',
    },
    warning: {
      main: '#ffa000',
      contrastText: '#fff',
    },
    success: {
      main: '#43a047',
      contrastText: '#fff',
    },
    info: {
      main: '#1976d2',
      contrastText: '#fff',
    },
    background: {
      main: '#EEEEEE',
    },
  },
  typography: {
    useNextVariants: true,
  },
});

export const orangeTheme = createMuiTheme({
  palette: {
    primary: {
      light: '#de8415',
      main: '#a5610d',
      dark: '#a05d0a',
      contrastText: '#fff',
    },
    secondary: {
      light: '#deab17',
      main: '#b58a0f',
      dark: '#9e7a10',
      contrastText: '#fff',
    },
    warning: {
      main: '#ffa000',
      contrastText: '#fff',
    },
    success: {
      main: '#43a047',
      contrastText: '#fff',
    },
    info: {
      main: '#1976d2',
      contrastText: '#fff',
    },
    background: {
      main: '#EEEEEE',
    },
  },
  typography: {
    useNextVariants: true,
  },
});

export const blueTheme = createMuiTheme({
  palette: {
    primary: {
      light: '#3584d6',
      main: '#255c94',
      dark: '#173d65',
      contrastText: '#fff',
    },
    secondary: {
      light: '#4d95d4',
      main: '#356996',
      dark: '#20405d',
      contrastText: '#fff',
    },
    warning: {
      main: '#ffa000',
      contrastText: '#fff',
    },
    success: {
      main: '#43a047',
      contrastText: '#fff',
    },
    info: {
      main: '#1976d2',
      contrastText: '#fff',
    },
    background: {
      main: '#EEEEEE',
    },
  },
  typography: {
    useNextVariants: true,
  },
});

export const blackTheme = createMuiTheme({
  palette: {
    primary: {
      light: '#565353',
      main: '#272525',
      dark: '#000000',
      contrastText: '#fff',
    },
    secondary: {
      light: '#414b5d',
      main: '#29303c',
      dark: '#1e232b',
      contrastText: '#fff',
    },
    warning: {
      main: '#ffa000',
      contrastText: '#fff',
    },
    success: {
      main: '#43a047',
      contrastText: '#fff',
    },
    info: {
      main: '#1976d2',
      contrastText: '#fff',
    },
    background: {
      main: '#EEEEEE',
    },
  },
  typography: {
    useNextVariants: true,
  },
});
