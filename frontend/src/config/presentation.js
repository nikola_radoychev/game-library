export const getStepContent = step => {
  switch (step) {
    case 0:
      return {
        text: [
          'Backend is separated in three sections app, models, controllers.',
          'In the app folder we have the main router.',
          'In the controllers I separate each model actions in a different folder. The folder has index with the routes for the current model and a controller to manipulate the database.',
          'I call the controllers into the current route and import the route for the model in the main route - app/routes.js',
        ],
        image: '/presentation/1.png',
      };
    case 1:
      return {
        text: [
          'Passport + Jwt',
          'Implement two strategies - LocalStrategy and JwtStrategy.',
          'Implement a special route for authentication - auth/profile.',
          'The jwt secret is in config.js',
        ],
        image: '/presentation/2.png',
      };
    case 2:
      return {
        text: [
          'Frontend is separated in four sections.',
          'Components - implement the React components',
          'config - Implement the constants and the configurations of the theme for example.',
          'Rematch-models - Here are the functions for the global state. I use "redux" + "rematch" to manipulate the global state.',
          'The fourth section are the files in the main directory.',
          'AppRouter is a React component which implements the routes.',
          'localStorage.js + store.js where I configure the global state and configure which part of it stays in the Local Storage of the browser.',
          'I provide the store through the App.js with the provider from "react-redux".',
        ],
        image: '/presentation/3.png',
      };
    case 3:
      return {
        text: [
          'Material ui is the app’s UI.',
          'I have a few color themes in config/themes.js.',
          'Thanks to Material UI I have a great responsive design.',
          'I have great and easy styling possibilities thanks to the component "withStyles" from matherialui/core/styles.',
          'I have a lot of helpful components which are easy to override.',
        ],
        image: '/presentation/4.png',
      };
    case 4:
      return {
        text: [
          'I have an object with two properties - reducers and effects which manipulate the global state.',
          'All the requests to restApi are made from the rematch models’ functions.',
          'The best practice in rematch for the reducers is the simplicity. They have the state of the model as a first parameter and the payload as a second one.',
          'I usually called the reducers after an effect usage.',
          'The effects have two parameters: the first one is the payload and the second one - the rootState, which is the whole global state.',
          'Thanks to the dispatch function we can easily access the other rematch models.',
        ],
        image: '/presentation/5.png',
      };
    case 5:
      return {
        text: [
          'I use functional and class components - when I need a local component state.',
          'I  easily combine the react components with the rematch model’s functions thanks to the module "connect" from react-redux.',
          'I passed the data from the global state. And I also passed the functions from the rematch reducers and effects thanks to the functions "mapState" and "mapDispatch" which are the parameters of the "connect" module.',
          "The functions return objects which are assigned to the component's props.",
        ],
        image: '/presentation/6.png',
      };
    case 6:
      return {
        text: [
          'Users have two collections of games. They can add and remove items from the collections.',
          'The homepage is designed to show which games the user likes and the ones that the user wants to play.',
          'The users can post reviews and see their profile page.',
          'The users can view the single game details.',
          'These are the ordinary user’s functions.',
        ],
        image: '/presentation/7.png',
      };
    case 7:
      return {
        text: [
          'Admin, Moderator and User.',
          'I described the users functionality above.',
          'In the user profile there are three sections. The “User” has only access to the first one.',
          'The moderator has access to the first and the second one.',
          'The Admin has full access.',
          'Moderators can edit games, add new games and delete games.',
          'Admins can change the user roles and delete users.',
          'If a user without access tries to manipulate the browser, the backend will prevent him from doing so. The app won\'t break up but a well-designed error message for insufficient will appear through the "rematch" models.',
        ],
        image: '/presentation/8.png',
      };
    case 8:
      return {
        text: [
          'There is a global component - <Notifications/> imported in the layout which shows the errors and the notifications.',
          'Each or at least the most of the notifications and the errors are passed through the rematch models.',
        ],
        image: '/presentation/9.png',
      };
    case 9:
      return {
        text: [
          'I check and validate the forms in the rematch models.',
          'For the register and the login form (SignIn and SignUp) I use the npm validator to check the email. The validation and the notifications are made by the rematch models and the global component for notifications.',
          'I have three more forms. For two of them I use a great "Dialog" component from material UI.',
        ],
        image: '/presentation/10.png',
      };
    case 10:
      return {
        text: [
          'The home page displays all the games.',
          'The favourite games page uses a generic table component and displays the favourite games of the user.',
          'The page with the games which the user wants to play is the same with different data.',
          'There are single game pages with details.',
          'User panel has three pages on the same route: UserInfo , ManageGames and ManageUsers, with access according to the user role.',
          'The presentation page is on the route /about.',
        ],
        image: '/presentation/11.png',
      };
    default:
      return 'Unknown step';
  }
};
