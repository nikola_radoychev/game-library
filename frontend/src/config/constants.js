const adminFunctions = [
  'View all games',
  'View game details',
  'Access favourite games collection',
  'Access wants to play games collection',
  'Add reviews',
  'Create games',
  'Edit Games',
  'Delete games',
  'Delete users',
  'Change user roles',
  'View all users',
];
const moderatorFunctions = adminFunctions.slice(0, 7);
const userFunctions = adminFunctions.slice(0, 5);

export const accountFunctions = {
  admin: adminFunctions,
  moderator: moderatorFunctions,
  user: userFunctions,
};
