# Game community

# Components

- Backend API (NodeJS + Express)
- Frontend (React + Redux + Rematch)

# Weapons of Choice

- Programming language: **JavaScript** (ES6+) (NodeJS runtime 10+) (Babel)
- Package manager: **yarn**
- Database: **MongoDB**
  - ORM - Mongoose
- UI - MaterialUI

# Project configuration

- Install dependencies
  - /backend yarn install --frozen-lockfile
  - /fronend yarn install --frozen-lockfile
- Start backend: yarn run server
  - port: 5050
- Start frontend: yarn start
- Presentation - implemented in the project: route - '/about'
-

# App configuration

- Admin user
  - The first registered user
  - The first registered user if there isn't a user with 'Admin' role
  - The one with editted role by another Admin
