import mongoose from 'mongoose';
import { Schema } from 'mongoose';

const reviewSchema = new Schema({
  content: {
    type: String,
    required: true,
  },
  creator: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  game: {
    type: Schema.Types.ObjectId,
    ref: 'Game',
  },
});

export default mongoose.model('Review', reviewSchema);
