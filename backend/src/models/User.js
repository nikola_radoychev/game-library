import mongoose from 'mongoose';
import { Schema } from 'mongoose';
import encription from '../controllers/auth/encription';

const userSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  passwordHash: {
    type: String,
    required: true,
  },
  passwordSalt: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: {
      values: ['Admin', 'Moderator', 'User'],
      message:
        'Status is invalid, valid values include [Admin, Moderator, User].',
    },
    default: 'User',
    required: true,
  },
  reviews: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Review',
    },
  ],
  favouriteGames: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Game',
    },
  ],
  wantsToPlay: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Game',
    },
  ],
});

userSchema.method({
  authenticate: (user, password) => {
    return (
      encription.generateHashedPassword(user.passwordSalt, password) ===
      user.passwordHash
    );
  },
});

const User = mongoose.model('User', userSchema);

export default User;
