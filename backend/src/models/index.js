import Game from './Game';
import User from './User';
import Review from './Review';

const db = { Game, Review, User };

export default db;
