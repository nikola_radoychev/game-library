import mongoose from 'mongoose';
mongoose.Promise = global.Promise;
module.exports = () => {
  mongoose.connect('mongodb://localhost:27017/game-library', {
    useNewUrlParser: true,
  });
  const db = mongoose.connection;
  db.once('open', err => {
    if (err) {
      console.log(err);
    }
    console.log('Using MongoDB...');
    console.log('Database ready...');
  });

  db.on('error', reason => {
    console.log(reason);
  });
};
