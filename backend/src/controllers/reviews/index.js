import express from 'express';
import passport from 'passport';
import db from '../../models';
import {
  createReview,
  updateReview,
  fetchReviewsByGameId,
  deleteReview,
} from './controller';
const router = express.Router();

router.post(
  '/create',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const result = await createReview(db, req.user._id, req.body);
      res.status(201).json(result);
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  }
);

router.patch(
  '/update',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const result = await updateReview(db, req.body);
      res.status(201).json(result);
      return;
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  }
);

router.get('/game/:id/', async (req, res) => {
  try {
    const games = await fetchReviewsByGameId(db, req.params);
    res.status(201).json(games);
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }
});

router.delete(
  '/:id/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      if (req.user.role !== 'User') {
        const result = await deleteReview(db, req.params);
        res.status(201).json(result);
        return;
      }
      res.status(500).json({ error: 'Insufficient acces' });
    } catch (err) {
      res.status(400).json({ error: err.toString() });
    }
  }
);

export default router;
