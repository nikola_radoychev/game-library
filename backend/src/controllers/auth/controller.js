export const authenticateUser = async (db, data) => {
  const { email, password } = data;
  const user = await db.User.findOne({
    email,
  });
  if (user) {
    const valid = await user.authenticate(user, password);
    if (valid) {
      return {
        ...user,
        id: user._id,
        passwordHash: undefined,
        passwordSalt: undefined,
      };
    }
  }

  return null;
};
